import time
from functools import wraps
import random

def logging_decorator(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        start_tm_obj = time.localtime()
        start_tm = time.strftime('%Y-%m-%d %H:%M:%S',start_tm_obj)
        #print(start_tm)
        print("Function '{}' started execution: {}".format(fn.__name__, start_tm))
        print('Function arguments {} {} '.format(args, kwargs))
        result = fn(*args, **kwargs)
        print('Addition result: {}'.format(result))
        end_time = time.perf_counter()
        end_tm_obj = time.localtime()
        end_tm = time.strftime('%Y-%m-%d %H:%M:%S',end_tm_obj)
        #print(end_tm)
        print('Function {} ended execution: {} '.format(fn.__name__,end_tm))
        total_time = end_time - start_time
        print('Total execution time : {:.8f}'.format(total_time))
        print('******************************************************************')
        print()

    return wrapper

@logging_decorator
def add(list_of_args):
    sum = 0
    for i in list_of_args:
        sum += i

    return sum


@logging_decorator
def multiply(a, b):
    return a * b

print(add.__name__)
print(multiply.__name__)

random_list = [ random.randint(1,100) for i in range(100) ]
add(random_list)
multiply(2,4)
    